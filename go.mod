module display

go 1.12

require (
	github.com/GeertJohan/go.rice v1.0.0
	github.com/gorilla/websocket v1.4.0
)
