//go:generate rice embed-go

package display

import (
	"fmt"
	"image/color"
	"log"
	"net/http"
	"sort"
	"sync"

	rice "github.com/GeertJohan/go.rice"
	"github.com/gorilla/websocket"
)

type box struct {
	Key   string `json:"key"`
	W     int    `json:"w"`
	H     int    `json:"h"`
	X     int    `json:"x"`
	Y     int    `json:"y"`
	Color string `json:"color"`
}

type Box struct {
	Key                 string
	Width, Height, X, Y int
	Color               color.RGBA
}

func (b *Box) tojsonBox() *box {
	return &box{
		Key:   b.Key,
		W:     b.Width,
		H:     b.Height,
		X:     b.X,
		Y:     b.Y,
		Color: fmt.Sprintf("#%02x%02x%02x%02x", b.Color.R, b.Color.G, b.Color.B, 255-b.Color.A),
	}
}

type sendbox struct {
	Elements []*box `json:"elements"`
}

type Handler struct {
	fs    http.Handler
	boxes map[string]*Box
	mtx   sync.RWMutex
	cond  *sync.Cond
	buf   sendbox
}

// New creates a new display handler
func New() *Handler {
	h := &Handler{
		fs:    http.FileServer(rice.MustFindBox("static/build").HTTPBox()),
		boxes: make(map[string]*Box),
	}
	h.cond = sync.NewCond(h.mtx.RLocker())
	return h
}

var upgrader = websocket.Upgrader{}

// ServeHTTP implements http.Handler
func (h *Handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	log.Println(r.URL.Path)
	if r.URL.Path == "/ws" {
		conn, err := upgrader.Upgrade(w, r, nil)
		if err != nil {
			log.Println(err)
			return
		}

		h.mtx.RLock()
		for {
			if err := conn.WriteJSON(h.buf); err != nil {
				log.Println("error writing to socket", err)
				h.cond.L.Unlock()
				conn.Close()
				return
			}
			h.cond.Wait()
		}
	}
	h.fs.ServeHTTP(w, r)
}

// Set sets or updates a box in the display.
// if a box with the given key already exists, it is overwritten
// boxes are drawn in order of the keys
func (h *Handler) Set(box Box) {
	h.mtx.Lock()
	h.boxes[box.Key] = &box
	h.mtx.Unlock()
}

// Set removes the box box with the given key
func (h *Handler) Remove(key string) {
	h.mtx.Lock()
	delete(h.boxes, key)
	h.mtx.Unlock()
}

// Update the view after beeing done with the changes
func (h *Handler) Update() {
	h.mtx.Lock()
	h.buf.Elements = h.buf.Elements[:0]
	for _, b := range h.boxes {
		h.buf.Elements = append(h.buf.Elements, b.tojsonBox())
	}
	sort.Slice(h.buf.Elements, func(i, j int) bool { return h.buf.Elements[i].Key < h.buf.Elements[j].Key })
	h.cond.Broadcast()
	h.mtx.Unlock()
}
