import React, { Component } from 'react';
import './App.css';

class Box extends Component{
  constructor(props){
    super(props);
    this.props = props;
  }

  render(){
    var state = this.props.state;
    return <div style={{
      "backgroundColor":state.color,
      "position":"fixed",
      "width":state.w+"px",
      "height":state.h+"px",
      "top":state.y+"px",
      "left":state.x+"px",
    }}></div>
    
  }

}

class App extends Component {

  constructor(props) {
    super(props);
    this.updateStatus = this.updateStatus.bind(this);
    this.render = this.render.bind(this);
    this.state = { "elements": [] };
    this.reconnect = this.reconnect.bind(this);
    this.reconnect()
  }

  reconnect() {
    var proto  = "wss";
    var location = window.location
    if (location.protocol === "http:"){
      proto = "ws";
    }
    var url = proto+'://'+location.hostname+(location.port ? ':'+location.port: '')+location.pathname+"ws";
    console.info(url)
    clearTimeout();
    console.info("trying to connect to websocket")
    this.sock = new WebSocket(url);
    this.sock.onmessage = function (e) {
      this.updateStatus(JSON.parse(e.data));
    }.bind(this);
    this.sock.onclose = function(){
      this.sock = null;
      setTimeout(this.reconnect(),2000);
    }.bind(this)
    this.sock.onopen = function(){
      this.connecting = false;
      console.info("connected")
    }
  }
  updateStatus(data) {
    console.info(data);
    this.setState(data);
  }




  render() {
    return (
      <div>
        {this.state["elements"] ? this.state["elements"].map(function(elem){
          return  <Box state={elem}></Box>

        }) : [] }
      </div>
    );
  }
}

export default App;
