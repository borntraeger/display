package main

import (
	"display"
	"image/color"
	"net/http"
	"time"
)

func main() {
	d := display.New()
	http.Handle("/", d)
	go func() {
		left := 100
		for {
			time.Sleep(time.Second)
			d.Set(display.Box{
				X:      100,
				Y:      150 + left,
				Width:  200,
				Height: 150,
				Key:    "TEST",
				Color:  color.RGBA{255, 0, 0, 0},
			})

			left *= -1
			d.Update()
		}
	}()
	_ = http.ListenAndServe(":8080", nil)

}
